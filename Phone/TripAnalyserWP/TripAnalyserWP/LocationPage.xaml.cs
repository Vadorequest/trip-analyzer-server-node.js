﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TripAnalyserWP.Model;
using Microsoft.Phone.Maps.Controls;
using System.Device.Location; // Provides the GeoCoordinate class.
using System.Windows.Media;
using System.Windows.Shapes;
using TripAnalyserWP.Design;
using TripAnalyserWP.Design.Session;
using System.Diagnostics;

namespace TripAnalyserWP
{
    public partial class LocationPage : PhoneApplicationPage
    {
        private DataDevice device;

        MapLayer myLocationLayer;

        List<string> listLocationIdDisplayed = new List<string>();

        // Time elapsed before auto refresh.
        private int secBeforeRefresh = 5;

        public LocationPage()
        {
            InitializeComponent();

            // Get parameters.
            this.device = (DataDevice)NavigationService.GetLastNavigationData();
            this.myLocationLayer = new MapLayer();

            ShowLastLocations();

            // Start new thread in background.
            var refreshMapBackground = new System.Threading.Thread(RefreshMapBackground);
            refreshMapBackground.Start();
        }

        public async void ShowLastLocations()
        {
            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", this.device.HardwareId));// Mandatory.
            parameters.Add(new Param("limit", "10"));

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("location", "find", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                JsonArray locations = (JsonArray)data["location"];
                String tyy = locations[0].GetType().ToString();

                int i = 0;
                
                foreach (JsonObject loc in locations)
                {
                    double lat = double.Parse((String)loc["lat"]);
                    double lon = double.Parse((String)loc["long"]);
                    GeoCoordinate coord = new GeoCoordinate(lat, lon);
                    Ellipse myCircle = new Ellipse();
                    if (i == 0)
                    {
                        this.deviceMap.Center = coord;
                        this.deviceMap.ZoomLevel = UserSession.Actions.Count > 0 && UserSession.Actions.Last().Type == "testPredefined" ? 15 : 2;
                        myCircle.Fill = new SolidColorBrush(Colors.Blue);
                    }
                    else
                    {
                        myCircle.Fill = new SolidColorBrush(Colors.Red);
                    }
                    myCircle.Height = 15;
                    myCircle.Width = 15;
                    myCircle.Opacity = 50;
                    // Create a MapOverlay to contain the circle.
                    MapOverlay myLocationOverlay = new MapOverlay();
                    myLocationOverlay.Content = myCircle;
                    myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
                    myLocationOverlay.GeoCoordinate = coord;
                    myLocationLayer.Insert(0, myLocationOverlay);// Last point is at the first position.

                    // Add the location to the list of displayed location.
                    this.listLocationIdDisplayed.Add((string)loc["id"]);

                    i++;
                }

                this.deviceMap.Layers.Add(myLocationLayer);
            }
        }

        private void mapZoomPlus_Click(object sender, RoutedEventArgs e)
        {
            this.deviceMap.ZoomLevel += 1;
        }

        private void mapZoomMinus_Click(object sender, RoutedEventArgs e)
        {
            if (this.deviceMap.ZoomLevel > 1)
            {
                this.deviceMap.ZoomLevel -= 1;
            }            
        }

        // Function called in a separate thread which refresh the Map.
        private async void RefreshMapBackground()
        {
            while (true)
            {
                // Sleep some time.
                System.Threading.Thread.Sleep(this.secBeforeRefresh * 1000);

                try
                {
                    List<Param> parameters = new List<Param>();
                    parameters.Add(new Param("email", UserSession.Email));
                    parameters.Add(new Param("password", UserSession.Password));
                    parameters.Add(new Param("hardwareId", this.device.HardwareId));// Mandatory.
                    parameters.Add(new Param("limit", "1"));

                    // Send the request.
                    IDictionary<string, object> response = await WebService.Send("location", "find", parameters);

                    if ((bool)response["status"])
                    {
                        // Get objects from the response.
                        Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                        // We got an array, convert to array.
                        JsonArray locationsArray = (JsonArray)data["location"];

                        // Add the location to the map.
                        locationsArray.ForEach(delegate(dynamic location)
                        {
                            if ((string)location["id"] != this.listLocationIdDisplayed.Last())
                            {
                                // Update UI thread.
                                Deployment.Current.Dispatcher.BeginInvoke(() =>
                                {
                                    double lat = double.Parse((String)location["lat"]);
                                    double lon = double.Parse((String)location["long"]);
                                    GeoCoordinate coord = new GeoCoordinate(lat, lon);

                                    Ellipse myCircle = new Ellipse();
                                    myCircle.Fill = new SolidColorBrush(Colors.Blue);
                                    myCircle.Height = 15;
                                    myCircle.Width = 15;
                                    myCircle.Opacity = 50;
                                    // Create a MapOverlay to contain the circle.
                                    MapOverlay myLocationOverlay = new MapOverlay();
                                    myLocationOverlay.Content = myCircle;
                                    myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
                                    myLocationOverlay.GeoCoordinate = coord;

                                    // Update the color of the last point.
                                    ((Ellipse)myLocationLayer.First().Content).Fill = new SolidColorBrush(Colors.Red);
                                    ((Ellipse)myLocationLayer.Last().Content).Fill = new SolidColorBrush(Colors.Red);
                                    myLocationLayer.Insert(0, myLocationOverlay);// Last point is at the first position.

                                    // Add the location to the list of displayed location.
                                    this.listLocationIdDisplayed.Add((string)location["id"]);
                                });
                            }// Else, location already displayed.
                        });

                    }
                    else
                    {
                        Debug.WriteLine((string)response["message"]);
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Unable to refresh, another thread has updated the device list. " + e.Message);
                }

            }
        }
    }
}