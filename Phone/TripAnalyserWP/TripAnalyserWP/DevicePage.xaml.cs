﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TripAnalyserWP.Design.Session;
using TripAnalyserWP.Model;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Diagnostics;
using TripAnalyserWP.Design;
using System.Threading;

namespace TripAnalyserWP
{
    public partial class DevicePage : PhoneApplicationPage
    {
        // Time elapsed before auto refresh.
        private int secBeforeRefresh = 5;

        public DevicePage()
        {
            InitializeComponent();

            // Display message waiting before make the http query.
            txtMessage.Foreground = new SolidColorBrush(Colors.Orange);
            txtMessage.Text = "Loading...";

            // Initialize the view data binding.
            InitializeAsynch();

            // Start new thread in background.
            var refreshViewBackground = new System.Threading.Thread(RefreshViewBackground);
            refreshViewBackground.Start();
        }

        private async void InitializeAsynch()
        {
            // TODO BAD CODE but I don't understand (and I don't have enough time) how make it better. http://blog.stephencleary.com/2013/01/async-oop-2-constructors.html
            DataContext = await UserSession.RefreshLastLocationDevices();
            // If I do that now with empty session, the view will be empty.
            //DataContextAction.ItemsSource = UserSession.Actions;
            txtMessage.Text = "";
        }

        private void btnUpdateDevice_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            DataDevice device = (DataDevice)_button.CommandParameter;

            NavigationService.Navigate("/DeviceFormPage.xaml", device);
        }

        // Change view, display a map.
        private void btnGoMap_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            DataDevice device = (DataDevice)_button.CommandParameter;

            txtMessage.Foreground = new SolidColorBrush(Colors.Orange);
            txtMessage.Text = "Map loading, please wait...";
            
            NavigationService.Navigate("/LocationPage.xaml", device);
        }

        // Run predefined test.
        private async void btnTestPredefined_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            DataDevice device = (DataDevice)_button.CommandParameter;

            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", device.HardwareId));// Mandatory.
            parameters.Add(new Param("type", "testPredefined"));

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("action", "create", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                Dictionary<string, object> action = (Dictionary<string, object>)data["action"];

                DataAction actionObject = new DataAction((string)action["id"], (string)action["hardwareId"], (string)action["type"], (string)action["status"]);
                UserSession.AddAction(actionObject);

                // Start listening action.
                StartListenNewAction(actionObject, device);

                // Display message.
                txtMessage.Foreground = new SolidColorBrush(Colors.Orange);
                txtMessage.Text = "Test with prefefined locations is pending.";
            }
            else
            {
                // TODO Display error
                txtMessage.Foreground = new SolidColorBrush(Colors.Red);
                txtMessage.Text = (string)response["message"];
            }
        }

        // Run random test.
        private async void btnTestRandom_Click(object sender, RoutedEventArgs e)
        {
            Button _button = (Button)sender;
            DataDevice device = (DataDevice)_button.CommandParameter;

            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", device.HardwareId));// Mandatory.
            parameters.Add(new Param("type", "testRandom"));

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("action", "create", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                Dictionary<string, object> action = (Dictionary<string, object>)data["action"];

                DataAction actionObject = new DataAction((string)action["id"], (string)action["hardwareId"], (string)action["type"], (string)action["status"]);
                UserSession.AddAction(actionObject);
                
                // Start listening action.
                StartListenNewAction(actionObject, device);

                // Display message.
                txtMessage.Foreground = new SolidColorBrush(Colors.Orange);
                txtMessage.Text = "Test with random locations is pending.";
            }
            else
            {
                // TODO Display error
                txtMessage.Foreground = new SolidColorBrush(Colors.Red);
                txtMessage.Text = (string)response["message"];
            }
        }

        // Function called in a separate thread which refresh DataContext, the devices.
        private async void RefreshViewBackground()
        {
            while (true)
            {
                // Sleep some time.
                System.Threading.Thread.Sleep(this.secBeforeRefresh * 1000);

                try
                {
                    // Get device locations.
                    List<DataDevice> devices = await UserSession.RefreshLastLocationDevices();

                    // Update UI thread.
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        DataContext = devices;
                    });
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Unable to refresh, another thread has updated the device list. " + e.Message);
                }

            }
        }

        private void StartListenNewAction(DataAction action, DataDevice device)
        {
            //// Update the actions list;
            //try
            //{
            //    Deployment.Current.Dispatcher.BeginInvoke(() =>
            //    {
            //         DataContextAction.ItemsSource = UserSession.Actions;
            //    });
            //}
            //catch (Exception e)
            //{
            //    Debug.WriteLine("Unable to refresh, another thread has updated the action list. " + e.Message);
            //}

            // Start new thread in background for this specific action.
            ParameterizedThreadStart refreshActionBackground = new ParameterizedThreadStart(RefreshActionBackground);
            Thread thread = new Thread(refreshActionBackground);
            thread.Start(new object[] { action, device });
        }

        // Refresh only one action in background.
        public async void RefreshActionBackground(object args)
        {
            bool pending = true;

            DataAction action = (DataAction)((object[])args)[0];
            DataDevice device = (DataDevice)((object[])args)[1];

            while (pending)
            {
                // Sleep some time.
                System.Threading.Thread.Sleep(device.AskingFrequencyActive * 1000);
                
                // Get the action from the web service.
                List<Param> parameters = new List<Param>();
                parameters.Add(new Param("email", UserSession.Email));
                parameters.Add(new Param("password", UserSession.Password));
                parameters.Add(new Param("hardwareId", device.HardwareId));// Mandatory.
                parameters.Add(new Param("id", action.Id));
                parameters.Add(new Param("limit", "1"));

                // Send the request.
                IDictionary<string, object> response = await WebService.Send("action", "find", parameters);

                if ((bool)response["status"])
                {
                    // Get objects from the response.
                    Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                    // We got an array, convert to array.
                    JsonArray actionsArray = (JsonArray)data["action"];

                    // Update the session.
                    actionsArray.ForEach(delegate(dynamic actionJson)
                    {
                        // Update the action from the session.
                        UserSession.Actions.Where(s => (string)s.Id == (string)actionJson["id"]).First().Status = (string)actionJson["status"];

                        // Get the action from the session.
                        DataAction actionSession = UserSession.Actions.Where(s => s.Id == action.Id).First();

                        // Compare to the session which could be updated since the last time.
                        if (actionSession.Status == "Executed")
                        {
                            pending = false;
                            // Update UI thread.
                            Deployment.Current.Dispatcher.BeginInvoke(() =>
                            {
                                //// Update the actions list;
                                //try
                                //{
                                //    DataContextAction.ItemsSource = UserSession.Actions;
                                //}catch(Exception e){
                                //    Debug.WriteLine("Unable to refresh, another thread has updated the action list. " + e.Message);
                                //}

                                // Update UI interface.
                                txtMessage.Foreground = new SolidColorBrush(Colors.Green);
                                this.txtMessage.Text = "Action executed: " + action.Type;
                            });
                        }
                    });

                }
                else
                {
                    Debug.WriteLine((string)response["message"]);
                }
            }
        }

        private async void btnAddDevice_Click(object sender, RoutedEventArgs e)
        {
            // Display message before update.
            txtMessage.Foreground = new SolidColorBrush(Colors.Orange);
            txtMessage.Text = "Device creating...";

            string hardwareId = txtHardwareId.Text,
                label = txtLabel.Text,
                details = txtDetails.Text,
                refreshFrequency = txtRefreshFrequency.Text,
                askingFrequency = txtAskingFrequency.Text,
                askingFrequencyActive = txtAskingFrequencyActive.Text;

            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", hardwareId));// Mandatory.

            if (label.Length > 0)
            {
                parameters.Add(new Param("label", label));
            }

            if (details.Length > 0)
            {
                parameters.Add(new Param("details", details));
            }

            if (refreshFrequency.Length > 0)
            {
                parameters.Add(new Param("refreshFrequency", refreshFrequency));
            }

            if (askingFrequency.Length > 0)
            {
                parameters.Add(new Param("askingFrequency", askingFrequency));
            }

            if (askingFrequencyActive.Length > 0)
            {
                parameters.Add(new Param("askingFrequencyActive", askingFrequencyActive));
            }

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("device", "create", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                dynamic device = data["device"];

                // Details is optional.
                try
                {
                    details = (string)device["details"];
                }
                catch (Exception exception)
                {
                    details = "";
                }

                // Save the device created in the session.
                UserSession.Devices.Add(new DataDevice(
                    (string)device["id"],
                    (string)device["hardwareId"],
                    (string)device["label"],
                    details,
                    (int)device["refreshFrequency"],
                    (int)device["askingFrequency"],
                    (int)device["askingFrequencyActive"]));


                // Display message even if it will disappear quicly.
                txtMessage.Foreground = new SolidColorBrush(Colors.Green);
                txtMessage.Text = (string)response["message"];
            }
            else
            {
                txtMessage.Foreground = new SolidColorBrush(Colors.Red);
                txtMessage.Text = (string)response["message"];
            }
        }
    }
}