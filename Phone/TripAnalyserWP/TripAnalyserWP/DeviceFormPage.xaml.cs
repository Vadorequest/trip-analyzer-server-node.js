﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TripAnalyserWP.Model;
using System.Diagnostics;
using TripAnalyserWP.Design.Session;
using TripAnalyserWP.Design;
using System.Windows.Media;

namespace TripAnalyserWP
{
    public partial class DeviceFormPage : PhoneApplicationPage
    {
        private DataDevice device;
        private bool edit;

        public DeviceFormPage()
        {
            InitializeComponent();

            // Get parameters.
            this.device = (DataDevice)NavigationService.GetLastNavigationData();
            this.edit = true;

            // Update the view with values.
            txtLabel.Text = device.Label;
            txtHardwareId.Text = device.HardwareId;
            txtDetails.Text = device.Details;
            txtRefreshFrequency.Text = device.RefreshFrequency.ToString();
            txtAskingFrequency.Text = device.AskingFrequency.ToString();
            txtAskingFrequencyActive.Text = device.AskingFrequencyActive.ToString();
        }

        private async void btnUpdateDevice_Click(object sender, RoutedEventArgs e)
        {
            // Display message before update.
            txtMessage.Foreground = new SolidColorBrush(Colors.Orange);
            txtMessage.Text = "Updating...";

            string hardwareId = txtHardwareId.Text,
                label = txtLabel.Text,
                details = txtDetails.Text,
                refreshFrequency = txtRefreshFrequency.Text,
                askingFrequency = txtAskingFrequency.Text,
                askingFrequencyActive = txtAskingFrequencyActive.Text;

            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", hardwareId));// Mandatory.

            if (label.Length > 0)
            {
                parameters.Add(new Param("label", label));
            }

            if (details.Length > 0)
            {
                parameters.Add(new Param("details", details));
            }

            if (refreshFrequency.Length > 0)
            {
                parameters.Add(new Param("refreshFrequency", refreshFrequency));
            }

            if (askingFrequency.Length > 0)
            {
                parameters.Add(new Param("askingFrequency", askingFrequency));
            }

            if (askingFrequencyActive.Length > 0)
            {
                parameters.Add(new Param("askingFrequencyActive", askingFrequencyActive));
            }

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("device", "update", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                dynamic device = data["device"];

                // Get the old object in session.
                DataDevice oldDevice = UserSession.Devices.Where(s => (string)s.Id == (string)device["id"]).First();

                // Get its last location by default.
                string lastLocation = oldDevice.LastLocation;

                // Update the session but be aware of the others threads using it.
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    // Remove it from the session.
                    UserSession.Devices.Remove(oldDevice);

                    // Details is optional.
                    try
                    {
                        details = (string)device["details"];
                    }
                    catch (Exception exception)
                    {
                        details = "";
                    }
                
                    // Save the device updated in the session.
                    UserSession.Devices.Add(new DataDevice(
                        (string)device["id"],
                        (string)device["hardwareId"],
                        (string)device["label"],
                        details,
                        (int)device["refreshFrequency"],
                        (int)device["askingFrequency"],
                        (int)device["askingFrequencyActive"],
                        lastLocation));
                });
                

                // Display message even if it will disappear quicly.
                txtMessage.Foreground = new SolidColorBrush(Colors.Green);
                txtMessage.Text = (string)response["message"];

                // Go to the previous view.
                NavigationService.GoBack();
            }
            else
            {
                txtMessage.Foreground = new SolidColorBrush(Colors.Red);
                txtMessage.Text = (string)response["message"];                
            }
        }
    }
}