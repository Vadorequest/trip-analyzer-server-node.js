﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TripAnalyserWP.Model;
using Newtonsoft.Json;
using System.Diagnostics;

namespace TripAnalyserWP.Design
{
    class WebService : HttpWebRequest
    {
        private static string uri = "http://tripanalyzer.cloudapp.net:1337/";

        /**
         * Send a request to the web service. Send a POST request with GET parameters, parse the JSON response in a dictionnary and return it asynchronously.
         */
        public static async Task<IDictionary<string, object>> Send(string controller, string action, List<Param> parameters)
        {
            // Generate the uri.
            string uri = WebService.uri + controller + "/" + action;
            if (parameters.Count > 0)
            {
                int i = 0;
                parameters.ForEach(delegate(Param param)
                {
                    uri += i == 0 ? "?" : "&";
                    uri += param.Key + "=" + param.Value;
                    i++;
                });
            }

            // Escape special chars.
            uri = Uri.EscapeUriString(uri);

            Debug.WriteLine("Query sent: " + uri);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = HttpMethod.Post;// Use POST, not GET because we will open a stream. (And get a streamViolation if use GET protocol)
            request.ContentType = "application/x-www-form-urlencoded";

            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
            using (var sr = new StreamReader(response.GetResponseStream()))
            using (JsonReader jsonreader = new JsonTextReader(sr))
            {
                string json = sr.ReadToEnd();
                Debug.WriteLine("Response: " + json);
                var jsonObject = (IDictionary<string, object>)SimpleJson.DeserializeObject(json);
                var dataContent = new Dictionary<string, object>();

                foreach (KeyValuePair<string, dynamic> dataPair in (IDictionary<string, object>)jsonObject["data"])
                {
                    var dataPairKey = dataPair.Key;
                    try
                    {
                        // Try to get a Dictionnary.
                        var dataSubContent = new Dictionary<string, object>();
                        foreach (KeyValuePair<string, dynamic> dataSubPair in (IDictionary<string, object>)dataPair.Value)
                        {
                            try
                            {
                                // Try to convert to dictionnary. Will fail if a string.
                                dataSubContent.Add(dataSubPair.Key, (IDictionary<string, object>)dataSubPair.Value);
                            }
                            catch (Exception e)
                            {
                                // It's a string (or something else but...).
                                dataSubContent.Add(dataSubPair.Key, dataSubPair.Value);
                            }
                        }

                        dataContent.Add(dataPair.Key, dataSubContent);
                    }catch(Exception eString){
                        // But it's maybe a string.
                        try
                        {
                            dataContent.Add(dataPair.Key, (string)dataPair.Value);
                        }
                        catch (Exception eArray)
                        {
                            // But it's maybe an array.
                            try
                            {
                                dataContent.Add(dataPair.Key, (JsonArray)dataPair.Value);
                            }
                            catch (Exception e)
                            {
                                // Or something else?
                                Debug.WriteLine(e.Message);
                            }
                        }
                    }
                }
                // Replace the data key.
                jsonObject.Remove("data");
                jsonObject.Add("data", dataContent);

                return jsonObject;
            }
        }
    }

    /**
     * Add methods to HttpWebRequest.
     */
    public static class HttpExtensions
    {
        public static Task<Stream> GetRequestStreamAsync(this HttpWebRequest request)
        {
            var taskComplete = new TaskCompletionSource<Stream>();
            request.BeginGetRequestStream(ar =>
            {
                Stream requestStream = request.EndGetRequestStream(ar);
                taskComplete.TrySetResult(requestStream);
            }, request);
            return taskComplete.Task;
        }

        public static Task<HttpWebResponse> GetResponseAsync(this HttpWebRequest request)
        {
            var taskComplete = new TaskCompletionSource<HttpWebResponse>();
            request.BeginGetResponse(asyncResponse =>
            {
                try
                {
                    HttpWebRequest responseRequest = (HttpWebRequest)asyncResponse.AsyncState;
                    HttpWebResponse someResponse = (HttpWebResponse)responseRequest.EndGetResponse(asyncResponse);
                    taskComplete.TrySetResult(someResponse);
                }
                catch (WebException webExc)
                {
                    HttpWebResponse failedResponse = (HttpWebResponse)webExc.Response;
                    taskComplete.TrySetResult(failedResponse);
                }
            }, request);
            return taskComplete.Task;
        }
    }

    public static class HttpMethod
    {
        public static string Head { get { return "HEAD"; } }
        public static string Post { get { return "POST"; } }
        public static string Put { get { return "PUT"; } }
        public static string Get { get { return "GET"; } }
        public static string Delete { get { return "DELETE"; } }
        public static string Trace { get { return "TRACE"; } }
        public static string Options { get { return "OPTIONS"; } }
        public static string Connect { get { return "CONNECT"; } }
        public static string Patch { get { return "PATCH"; } }
    }
}
