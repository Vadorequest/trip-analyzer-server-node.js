﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TripAnalyserWP.Model;

namespace TripAnalyserWP.Design.Session
{
    /**
     * Represents the current user of the application. Use static fields. 
     * 
     */
    class UserSession
    {
        public static string Id
        {
            get;
            set;
        }

        public static string Email
        {
            get;
            private set;
        }

        public static string Password
        {
            get;
            private set;
        }

        public static List<DataDevice> Devices
        {
            get;
            private set;
        }

        public static List<DataAction> Actions
        {
            get;
            private set;
        }

        public static void Set(string id, string email, string password)
        {
            UserSession.Id = id;
            UserSession.Email = email;
            UserSession.Password = password;

            // Set empty.
            UserSession.Devices = new List<DataDevice>();
            UserSession.Actions = new List<DataAction>();
        }

        public static int CountDevices()
        {
            return UserSession.Devices.Count;
        }

        public async static Task<bool> RefreshDevicesList()
        {
            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("device", "find", parameters);

            if ((bool)response["status"])
            {
                // Reset the old list.
                UserSession.Devices = new List<DataDevice>();

                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];

                // We got an array, convert to array.
                JsonArray devicesArray = (JsonArray)data["device"];
                devicesArray.ForEach(delegate(dynamic device)
                {
                    // Details is optional.
                    string details;
                    try
                    {
                         details = (string)device["details"];
                    }catch(Exception e){
                        details = "";
                    }

                    // Save the device in the session.
                    UserSession.Devices.Add(new DataDevice(
                        (string)device["id"], 
                        (string)device["hardwareId"], 
                        (string)device["label"],
                        details, 
                        (int)device["refreshFrequency"], 
                        (int)device["askingFrequency"],
                        (int)device["askingFrequencyActive"]));
                });

                return true;
            }
            else
            {
                // TODO Display error
                //txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                //txtResponse.Text = (string)response["message"];
                Debug.WriteLine("Error UserSession.RefreshDevicesList() : " + (string)response["message"]);
                return false;
            }
        }

        // Refresh last location of a device.
        public async static Task<List<DataDevice>> RefreshLastLocationDevices()
        {
            List<DataDevice> devices = UserSession.Devices;
            List<DataDevice> devices_temp = new List<DataDevice>();

            foreach(DataDevice device in devices){
                List<Param> parameters = new List<Param>();
                parameters.Add(new Param("email", UserSession.Email));
                parameters.Add(new Param("password", UserSession.Password));
                parameters.Add(new Param("hardwareId", device.HardwareId));// Mandatory.
                parameters.Add(new Param("limit", "1"));

                // Send the request.
                IDictionary<string, object> response = await WebService.Send("location", "find", parameters);

                if ((bool)response["status"])
                {
                    // Get objects from the response.
                    Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                    // We got an array, convert to array.
                    JsonArray locationsArray = (JsonArray)data["location"];

                    // Add the last location to the device.
                    locationsArray.ForEach(delegate(dynamic location)
                    {
                        // The table will contains only one entry because we set the limit to 1.
                        device.LastLocation = (string)location["address"];
                    });
                    devices_temp.Add(device);
                }
                else
                {
                    // TODO Display error
                    //txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                    //txtResponse.Text = (string)response["message"];
                    Debug.WriteLine((string)response["message"]);
                }
            }
            UserSession.Devices = devices_temp;
            return UserSession.Devices;
        }

        // Mutator.
        public static void AddAction(DataAction action)
        {
            UserSession.Actions.Add(action);
        }

        // Mutator.
        public static void RemoveAction(DataAction action)
        {
            UserSession.Actions.Remove(action);
        }
    }
}
