﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripAnalyserWP.Model
{
    // Represent a parameter in a HTTP request.
    class Param
    {
        public string Key
        {
            get;
            private set;
        }

        public string Value
        {
            get;
            private set;
        }

        public Param(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
