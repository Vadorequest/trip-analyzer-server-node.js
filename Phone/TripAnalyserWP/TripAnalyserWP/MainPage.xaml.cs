﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TripAnalyserWP.Resources;
using System.Diagnostics;
using TripAnalyserWP.Model;
using TripAnalyserWP.Design;
using System.Windows.Media;
using TripAnalyserWP.Design.Session;

namespace TripAnalyserWP
{
    public partial class MainPage : PhoneApplicationPage
    {
        private bool authenticating = false;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private async void Button_Authentication(object sender, RoutedEventArgs e)
        {
            // Disable several try at the same time.
            if (!this.authenticating)
            {
                this.authenticating = true;
                // Get values from textboxes.
                string email = txtEmail.Text;
                string password = txtPassword.Password;

                txtResponse.Foreground = new SolidColorBrush(Colors.Orange);
                txtResponse.Text = "Connecting...";

                // Add values in the parameters.
                List<Param> parameters = new List<Param>();
                parameters.Add(new Param("email", email));
                parameters.Add(new Param("password", password));

                // Send the request.
                IDictionary<string, object> response = await WebService.Send("user", "connect", parameters);


                if ((bool)response["status"])
                {
                    // Get objects from the response.
                    Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                    Dictionary<string, object> user = (Dictionary<string, object>)data["user"];

                    // Connect the user. Fill the session and display message.
                    UserSession.Set((string)user["id"], email, password);
                    txtResponse.Foreground = new SolidColorBrush(Colors.Green);
                    txtResponse.Text = (string)response["message"] + "\n" + "Wait please...";

                    bool getDevicesSuccess = await UserSession.RefreshDevicesList();

                    if (getDevicesSuccess)
                    {
                        // User's devices are now in the session.
                        NavigationService.Navigate(new Uri("/DevicePage.xaml", UriKind.Relative));
                    }
                    else
                    {
                        txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                        txtResponse.Text = "Unable to load your devices.";
                    }
                }
                else
                {
                    // Don't connect the user. Display the error message.
                    txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                    txtResponse.Text = (string)response["message"];
                }

                // Enable new try.
                this.authenticating = false;
            }
            else
            {
                txtResponse.Foreground = new SolidColorBrush(Colors.Orange);
                txtResponse.Text = "Authentication in progress, please wait.";
            }
        }

        // LES FONCTIONS SUIVANTES ONT ETE REALISEES EN ATTENDANT DE LES METTRE AU BON ENDROIT. NE PAS SUPPRIMER CES EXEMPLES. ON LE FERA A LA FIN.


        private async void Button_UpdateAccount(object sender, RoutedEventArgs e)
        {
            // TODO: Changer la provenance des valeurs, probablement dans une autre vue.

            // Get values from textboxes.
            string email = txtEmail.Text;
            string password = txtPassword.Password;

            // Add values in the parameters, optional values, added only if not empty.
            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));

            if (email.Length > 0)
            {
                parameters.Add(new Param("newEmail", email));
            }

            if (password.Length > 0)
            {
                parameters.Add(new Param("newPassword", password));
            }
            

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("user", "update", parameters);


            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                Dictionary<string, object> user = (Dictionary<string, object>)data["user"];

                // Update the user session and display message.
                UserSession.Set((string)user["id"], email, password);
                txtResponse.Foreground = new SolidColorBrush(Colors.Green);
                txtResponse.Text = (string)response["message"];
            }
            else
            {
                // Don't update the user. Display the error message.
                txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                txtResponse.Text = (string)response["message"];
            }
        }

        private async void Button_UpdateDevice(object sender, RoutedEventArgs e)
        {
            // TODO Changer la provenance des valeurs.
            string hardwareId = "14:58:69:FF:00", label = "Tracker 007", details = "", refreshFrequency = "15", askingFrequency = "5", askingFrequencyActive = "2";

            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", hardwareId));// Mandatory.

            if (label.Length > 0)
            {
                parameters.Add(new Param("label", label));
            }

            if (details.Length > 0)
            {
                parameters.Add(new Param("details", details));
            }

            if (refreshFrequency.Length > 0)
            {
                parameters.Add(new Param("refreshFrequency", refreshFrequency));
            }

            if (askingFrequency.Length > 0)
            {
                parameters.Add(new Param("askingFrequency", askingFrequency));
            }

            if (askingFrequencyActive.Length > 0)
            {
                parameters.Add(new Param("askingFrequencyActive", askingFrequencyActive));
            }

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("device", "update", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                Dictionary<string, object> device = (Dictionary<string, object>)data["device"];
                Debug.WriteLine((string)device["label"]);
            }
            else
            {
                // TODO Display error
                txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                txtResponse.Text = (string)response["message"];
            }
        }

        private async void Button_FindAllDevice(object sender, RoutedEventArgs e)
        {
            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("device", "find", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                // We got an array, convert to array.
                JsonArray devicesArray = (JsonArray)data["device"];
                devicesArray.ForEach(delegate(dynamic device) {
                    // TODO Faire un truc utile, dépendra du contexte.
                    Debug.WriteLine((string)device["id"]);
                });
                
            }
            else
            {
                // TODO Display error
                txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                txtResponse.Text = (string)response["message"];
            }
        }

        private async void FindAllLocation_Click(object sender, RoutedEventArgs e)
        {
            // TODO Changer la provenance des valeurs.
            string hardwareId = "14:58:69:FF:00", limit = "", id = "";

            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", hardwareId));// Mandatory.

            if (limit.Length > 0)
            {
                parameters.Add(new Param("limit", limit));
            }

            if (id.Length > 0)
            {
                parameters.Add(new Param("id", id));
            }

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("location", "find", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                // We got an array, convert to array.
                JsonArray locationsArray = (JsonArray)data["location"];

                // TODO Faire un truc utile, dépendra du contexte. (Tracer la map, afficher la position actuelle du device)
                locationsArray.ForEach(delegate(dynamic location)
                {
                    Debug.WriteLine("Lat: "+(string)location["lat"]);
                });

            }
            else
            {
                // TODO Display error
                txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                txtResponse.Text = (string)response["message"];
            }
        }

        private async void FindAllAction_Click(object sender, RoutedEventArgs e)
        {
            // TODO Changer la provenance des valeurs.
            string hardwareId = "14:58:69:FF:00", limit = "", id = "", status = "";

            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", hardwareId));// Mandatory.

            if (limit.Length > 0)
            {
                parameters.Add(new Param("limit", limit));
            }

            if (id.Length > 0)
            {
                parameters.Add(new Param("id", id));
            }

            if (status.Length > 0)
            {
                parameters.Add(new Param("status", status));
            }

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("action", "find", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                // We got an array, convert to array.
                JsonArray actionsArray = (JsonArray)data["action"];

                // TODO Faire un truc utile, dépendra du contexte. (Indiquer qu'une action a été effectuée/est en cours de réalisation, )
                actionsArray.ForEach(delegate(dynamic action)
                {
                    Debug.WriteLine("Status: " + (string)action["status"]);
                });

            }
            else
            {
                // TODO Display error
                txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                txtResponse.Text = (string)response["message"];
            }
        }

        private async void CreateAction_Click(object sender, RoutedEventArgs e)
        {
            // TODO Changer la provenance des valeurs.
            string hardwareId = "14:58:69:FF:00", type = "functionName", dataJson = "{'key': 'valueString'}";

            List<Param> parameters = new List<Param>();
            parameters.Add(new Param("email", UserSession.Email));
            parameters.Add(new Param("password", UserSession.Password));
            parameters.Add(new Param("hardwareId", hardwareId));// Mandatory.

            if (type.Length > 0)
            {
                parameters.Add(new Param("type", type));
            }

            if (dataJson.Length > 0)
            {
                parameters.Add(new Param("data", dataJson));
            }

            // Send the request.
            IDictionary<string, object> response = await WebService.Send("action", "create", parameters);

            if ((bool)response["status"])
            {
                // Get objects from the response.
                Dictionary<string, object> data = (Dictionary<string, object>)response["data"];
                Dictionary<string, object> action = (Dictionary<string, object>)data["action"];
                Debug.WriteLine("Id: " + (string)action["id"]);
                Debug.WriteLine("Status: " + (string)action["status"]);// Status of the action, not the status of the request!

            }
            else
            {
                // TODO Display error
                txtResponse.Foreground = new SolidColorBrush(Colors.Red);
                txtResponse.Text = (string)response["message"];
            }
        }
    }
}