﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripAnalyserWP.Model
{
    class DataAction
    {
        public string Id
        {
            get;
            private set;
        }

        public string HardwareId
        {
            get;
            private set;
        }

        public string Type
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public DataAction(string id, string hardwareId, string type, string status)
        {
            this.Id = id;
            this.HardwareId = hardwareId;
            this.Type = type;
            this.Status = status;
        }
    }
}
