﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TripAnalyserWP.Model
{
    class DataDevice
    {
        public string Id
        {
            get;
            private set;
        }

        public string HardwareId
        {
            get;
            private set;
        }

        public string Label
        {
            get;
            private set;
        }

        public string Details
        {
            get;
            private set;
        }

        public int RefreshFrequency
        {
            get;
            private set;
        }

        public int AskingFrequency
        {
            get;
            private set;
        }

        public int AskingFrequencyActive
        {
            get;
            private set;
        }

        public string LastLocation
        {
            get;
            set;
        }

        public DataDevice(string id, string hardwareId, string label, string details, int refreshFrequency, int askingFrequency, int askingFrequencyActive)
        {
            this.Id = id;
            this.HardwareId = hardwareId;
            this.Label = label;
            this.Details = details;
            this.RefreshFrequency = refreshFrequency;
            this.AskingFrequency = askingFrequency;
            this.AskingFrequencyActive = askingFrequencyActive;
        }

        public DataDevice(string id, string hardwareId, string label, string details, int refreshFrequency, int askingFrequency, int askingFrequencyActive, string lastLocation)
        {
            this.Id = id;
            this.HardwareId = hardwareId;
            this.Label = label;
            this.Details = details;
            this.RefreshFrequency = refreshFrequency;
            this.AskingFrequency = askingFrequency;
            this.AskingFrequencyActive = askingFrequencyActive;
            this.LastLocation = lastLocation;
        }
    }
}
