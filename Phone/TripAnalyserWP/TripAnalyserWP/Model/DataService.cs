﻿using System;
using TripAnalyserWP.Design;

namespace TripAnalyserWP.Model
{
    public class DataService : IDataService
    {
        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to connect to the actual data service

            new DesignDataService().GetData(callback);
        }
    }
}