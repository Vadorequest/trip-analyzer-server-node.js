# How install:
Afin de faire tourner cette beauté sous node.js vous allez devoir installer plusieurs choses:

* Git (Une version pour windows, et oui)
* Node.js
* Npm, le Node Package Manager qui sert à télécharger, installer et mettre à jour des packages.
* Des packages via npm, ouais, y en a un packet.

**EDIT: Et bah non, en fait vous avez juste besoin de GIT ! Pas besoin de node.js sur la machine, sauf si vous souhaitez l'installer pour le plaisir.
Vous pouvez trouver le serveur en ligne ici: [http://tripanalyzer.cloudapp.net:1337](http://137.135.176.144:1337)**


###1. [Installer git, 1.8.3](http://msysgit.github.com/)

Vérifiez que git est installé via la commande:
```
git
```

Vérifiez que la version est 1.8.3 via la commande:
```
git --version
```

*Si ça ne fonctionne pas, vous ne pourrez pas récupérer le contenu depuis le SVN.*
###2. Suivez ce [tuto](http://www.atinux.fr/2011/12/13/installer-node-js-sous-windows-npm-inclus/) pour installer node et npm, inutile de faire tout le tuto.
###3. Assurez vous que ça fonctionne:
```
npm -v
node -v
```
###4. Une fois que tout est installé (et que vous n'avez toujours rien fait d'intéressant) vous allez pouvoir récupérer le contenu du serveur.
```
git pull *adresse du serveur + LOGIN:PASSWORD dans l'url*
```
###5. On Installe les **node_modules** qui sont les modules référencés dans le package.json du projet.
*(Les commandes suivantes doivent être effectué dans le répertoire où est situé le code du serveur)*
```
npm install
```
Ca met du temps.
###6. Et enfin on lance le serveur:
```
sails lift
```
Une fois que le server est lancé (plein de lignes et il indique le port):
Il n'y a plus qu'à aller sur le [http://localhost:1337/](localhost:1337)

Voilà, c'est installé et configuré, vous allez en chier un peu mais ça serait pas drôle sinon.
Si vous errez, bah cherchez sur le net. Enfin, la base quoi.
*Bisous les copains.*