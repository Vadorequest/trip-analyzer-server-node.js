/**
 * Bootstrap
 *
 * An asynchronous boostrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#documentation
 */

module.exports.bootstrap = function (cb) {
    /**
     * ***************************************************************
     * *********** Import GLOBAL vars available everywhere ***********
     * ***************************************************************
     */

    /**
     * Information about globals vars bounded automatically.
     *
     * Node globals:
     * @static process      Instance of EventEmitter. http://nodejs.org/api/process.html
     *
     * Sails globals:
     * @static _            Lodash, tools for collections. http://lodash.com/
     */

    /**
     * Create all global vars using node base modules. Don't use the keyword 'var'.
     * @static fs           File storage. http://nodejs.org/api/fs.html
     * @static os           Tools about OS. http://nodejs.org/api/os.html
     * @static util         Tools. http://nodejs.org/api/util.html
     * @static http         Http query object. http://nodejs.org/api/http.html
     * @static path         Tools for handling and transforming file paths. http://nodejs.org/api/path.html
     */
    fs = require('fs'),
    os = require('os'),
    util = require('util'),
    http = require('http'),
    path = require('path');

    /**
     * Create all global vars using node external modules. Don't use the keyword 'var'.
     * @static bcrypt       Crypt tools. https://github.com/ncb000gt/node.bcrypt.js/
     */
    bcrypt = require('bcrypt'),
    httpget = require('http-get');

    /**
     * ***************************************************************
     * ********************** Set sockets listeners ******************
     * ***************************************************************
     */
    sails.io.on('connection', function(socket) {
        console.log('New user try to connect using socket! connection');
        socket.send('You are now connected to the socket! connection');

        socket.on('disconnection', function (value) {

            console.log('User disconnected.');
            socket.emit('message', 'You are disconnected!');
        });
    });

    // It's very important to trigger this callack method when you are finished
    // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
    cb();
};