/**
 * Allow any authenticated user.
 */
module.exports = function (req, res, callback) {

    // User is allowed, proceed to controller
    if (req.session.authenticated) {
        return callback();
    } else {
        // User is not allowed
        return res.json({message: "You are not permitted to perform this action. You have to connect to the platform before.", data: {}, status: false}, 403);
    }
};