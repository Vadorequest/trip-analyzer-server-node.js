/**
 * Auto connect user/device if the hardwareId is good.
 */
module.exports = function (req, res, callback) {
    var deviceAccess = [
        'location',
        'action',
        'device/find'
    ];

    var uri = req.target.controller+'/'+req.target.action;
    var i = 0;
    for(param in req.query){
        if(i == 0){
            uri += '?';
        }else{
            uri += '&';
        }
        uri += param+'='+req.query[param];
    }
    console.log(uri);

    if (req.session.authenticated) {
        // Accept user authentication.
        return callback();
    } else if (validator.check([validator.rulesEmail(req.param('email')), validator.rulesPassword(req.param('password'))]).errors.status){
        User.exists({email: req.param('email')}, function(err, user){
            if(err){
                return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
            }

            if(user){
                bcrypt.compare(req.param('password'), user.password, function(err, result) {
                    if(err){
                        res.json({message: 'Error.', data: {code: '99', err: err}, status: false});
                    }

                    if(result === true){
                        //req.session.authenticated = true;
                        req.session.user = user;
                        req.session.from = 'phone';

                        return callback();
                    }else{
                        return res.json({message: 'Bad password.', data: {code: '1002'}, status: false});
                    }
                });
            }else{
                return res.json({message: 'This user don\'t exists.', data: {code: '1001'}, status: false});
            }
        });
    } else if(validator.check([validator.rulesHardwareId(req.param('hardwareId'))]).errors.status && (_.contains(deviceAccess, req.target.controller) || _.contains(deviceAccess, req.target.controller+'/'+req.target.action))){
        // Maybe hardware try to connect, check its ID.
        Device.findOne({hardwareId: req.param('hardwareId')}, function(err, device){
            if(err){
                return res.json({message: "You are not permitted to perform this action. You have to connect to the platform before. [Wrong hardwareId or DB error]", data: {err: err}, status: false}, 403);
            }

            if(device){
                // Connect the device.
                //req.session.authenticated = true;// Don't connect the user, else it's a security breach.
                req.session.from = 'device';

                // Search for the device's owner.
                User.findOne({id: device.userId}, function(err, user){
                    if(err){
                        return res.json({message: "DB error.", data: {err: err}, status: false}, 403);
                    }

                    if(user){
                        // Add data about the user.
                        req.session.user = user;
                        return callback();
                    }else{
                        return res.json({message: "Device found but device's owner doesn't found.", data: {err: err}, status: false}, 403);
                    }
                });
            }else{
                return res.json({message: "You are not permitted to perform this action. You have to connect to the platform before. [Wrong hardwareId]", data: {err: err}, status: false}, 403);
            }
        });
    }else{
        return res.json({message: "You are not permitted to perform this action. You have to connect to the platform before. Try to use auto-connection using hardwareId or email/password.", data: {}, status: false}, 403);
    }
};