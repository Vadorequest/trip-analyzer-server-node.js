/**
 * Format
 *
 * @module		:: Format
 * @description	:: Contains helper for format handler.
 * TODO Not used for the moment, production only but could damage the work of Julien or Remi.
 */
module.exports = {

    /**
     *
     * @param       message
     * @param       data
     * @param       status
     * @returns     {{s: boolean, [m: string], [d: object]}}
     * @author      Vadorequest
     *
     * @example     With res:
     return res.json(format.response('Message to display', {results: 50}, true));// Will return an object with 3 properties
     return res.json(format.response(false, {}, true));// Will return an object with 1 property
     return res.json(format.response(false, false, true));// Will return an object with 1 property
     */
    response: function(message, data, status){
        // The response has to contains the status.
        var response = {
            s: !!status
        };

        if(message && message.length){
            // If message exists and not empty, add the message.
            response.m = message;
        }

        if(_.isObject(data) && !_.isEmpty(data)){
            // If data exists and not empty, add the data.
            response.d = data;
        }

        return response;
    }
};