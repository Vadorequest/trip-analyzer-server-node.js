/**
 * Device
 *
 * @module      :: Model
 * @description :: Device model. Manage user's devices.
 *
 */

module.exports = {

    attributes: {
        /**
         * Device owner.
         */
        userId: {
            type: 'string',
            minLength: 24,
            maxLength: 24,
            required: true,
            notEmpty: true
        },

        /**
         * Device label.
         */
        label: {
            type: 'string',
            minLength: 2,
            maxLength: 20,
            required: true,
            notEmpty: true
        },

        /**
         * Device hardware identifier.
         */
        hardwareId: {
            type: 'string',
            minLength: 5,
            maxLength: 20,
            required: true,
            notEmpty: true,
            unique: true
        },

        /**
         * Device details.
         */
        details: {
            type: 'string'
        },

        /**
         * Device auto refresh frequency for send lat/lng in seconds.
         */
        refreshFrequency: {
            type: 'integer',
            min: 5,
            max: 300,
            defaultsTo: 10,
            required: true,
            notEmpty: true
        },

        /**
         * Device auto refresh frequency for check actions to do in seconds.
         */
        askingFrequency: {
            type: 'integer',
            min: 1,
            max: 900,
            defaultsTo: 300,
            required: true,
            notEmpty: true
        },

        /**
         * Device auto refresh frequency for check actions to do when at least one action is Pending or Processing in seconds.
         */
        askingFrequencyActive: {
            type: 'integer',
            min: 1,
            max: 300,
            defaultsTo: 2,
            required: true,
            notEmpty: true
        }
    },

    /**
     * Check if the device already exists in the database.
     * @param device - Device object which has to contains data to find.
     * @param callback
     */
    exists: function(device, callback){
        Device.findOne(device, function (err, deviceFounded) {
            if (err){
                return callback(err);
            }

            if (deviceFounded){
                return callback(null, deviceFounded);
            }else{
                return callback (null, false);
            }
        });
    }

};
