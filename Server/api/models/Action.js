/**
 * Action
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

    attributes: {

       /**
        * MAC address of the device.
        */
        hardwareId: {
            type: 'string',
            minLength: 5,
            maxLength: 20,
            required: true,
            notEmpty: true
        },

       /**
        * Status:
        *    Pending
        *    Processing
        *    Executed
        */
        status: {
            type: 'string',
            minLength: 2,
            maxLength: 20,
            defaultsTo: 'Pending',
            required: true,
            notEmpty: true
        },

       /**
        * Function to call.
        */
        type: {
            type: 'string',
            minLength: 2,
            maxLength: 50,
            required: true,
            notEmpty: true
        },

       /**
        * Data. JSON. Used by the function.
        */
        data: {
            type: 'string'
        },

       /**
        * Time when the action was executed by the device.
        */
        datetimeExecuted: {
            type: 'datetime'
        }
    },

    /**
     * Auto executed before an action is updated.
     * @param action
     * @param callback
     */
    beforeUpdate: function(action, callback){
        Action.findOne({id: action.id}, function (err, actionFound) {
            if (err){
                return callback('DB error occurred.');
            }

            // Auto add the date of execution if it was not Executed but will be.
            if(action.status && action.status == 'Executed' && actionFound.status != 'Executed'){
                action.datetimeExecuted = new Date().toISOString();
            }

            return callback();
        });
    }
};
