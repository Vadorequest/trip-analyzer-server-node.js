/**
 * Location
 *
 * @module      :: Model
 * @description :: Location model. Manage device's locations.
 *
 */

module.exports = {

    attributes: {

        /**
         * MAC address of the device.
         */
        hardwareId: {
            type: 'string',
            minLength: 5,
            maxLength: 20,
            required: true,
            notEmpty: true
        },

        /**
         * Location latitude.
         */
        lat: {
            type: 'string',
            minLength: 3,
            maxLength: 30,
            required: true,
            notEmpty: true
        },

        /**
         * Location longitude.
         */
        long: {
            type: 'string',
            minLength: 3,
            maxLength: 30,
            required: true,
            notEmpty: true
        },

        /**
         * Location street.
         */
        street: {
            type: 'string'
        },

        /**
         * Location city.
         */
        city: {
            type: 'string'
        },

        /**
         * Location country.
         */
        country: {
            type: 'string'
        },

        /**
         * Location country.
         */
        address: {
            type: 'string'
        }
    },

    /**
     * Auto executed before a location is created.
     * @param location
     * @param callback
     */
    beforeCreate: function(location, callback){
        location.street = '';
        location.city = '';
        location.country = '';
        location.address = '';

        httpget.get({
            url: 'maps.googleapis.com/maps/api/geocode/json?oe=utf-8&sensor=false&latlng='+location.lat + ',' + location.long,
            bufferType: 'buffer'
        }, function(err, res) {
            if(err){
                console.log('Unable to make a HTTP get request to GMap geocode API: '+err);
                return callback();// We still want to save the location.
            }

            try{
                // Try to parse the response. Will fail if the data is send in multipart.
                response = JSON.parse(res.buffer);
                if(response.status === 'OK'){
                    location.address = response.results[0].formatted_address;
                    // TODO Ajouter le reste mais la flemme.
                }
            }catch(e){
                console.log('Unable to parse the result from Google geocode API: '+ e.message);
            }
            callback();
        });

    }

};
