/**
 * User
 *
 * @module      :: Model
 * @description :: User model. Manage users.
 *
 */

var bcrypt = require('bcrypt');

module.exports = {

    attributes: {
        /**
         * User email.
         */
        email: {
            type: 'email',
            minLength: 5,
            maxLength: 30,
            required: true,
            notEmpty: true,
            unique: true
        },

        /**
         * User password.
         */
        password: {
            type: 'string',
            minLength: 2,
            maxLength: 20,
            required: true,
            notEmpty: true
        },

        /**
         * Override toJSON instance method, delete password.
         * @returns User
         */
        toJSON: function() {
            var obj = this.toObject();
            delete obj.password;
            return obj;
        }

    },

    /**
     * Check if the user already exists in the database.
     * @param user - User object which has to contains data to find.
     * @param callback
     */
    exists: function(user, callback){
        User.findOne(user, function (err, userFounded) {
            if (err){
                return callback(err);
            }

            if (userFounded){
                return callback(null, userFounded);
            }else{
                return callback (null, false);
            }
        });
    },

    /**
     * Hash the user password.
     * @param user
     * @param callback
     */
    hashPassword: function(user, callback){
        if(user.password){
            // Hash the password.
            bcrypt.hash(user.password, 10, function(err, hash) {
                if(err){
                    return callback(err);
                }

                // Update the password with the hashed one.
                user.password = hash;

                callback(null, hash);
            });
        }else{
            console.log("No password send.");
            callback();
        }
    },

    /**
     * Hash the password and return it.
     * @param password
     * @returns {*}
     */
    hashPasswordSync: function(password){
        if(password){
            // Hash the password.
            return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
        }else{
            console.log("No password send.");
            return;
        }
    },

    /**
     * Auto executed before a user is created.
     * @param user
     * @param callback
     */
    beforeCreate: function(user, callback){
        User.findOne({ email: new RegExp("^" + user.email + "$", 'i') }, function (err, userFound) {
            if (err){
                return callback('DB error occurred.');
            }

            // Make sure the user exists
            if (userFound){
                return callback('Email already taken.');
            }

            User.hashPassword(user, callback);
        });
    },

    /**
     * Auto executed before an user is updated.
     * @param user
     * @param callback
     */
    beforeUpdate: function(user, callback){
        User.findOne(user, function (err, userFound) {
            if (err){
                return callback('DB error occurred.');
            }

            // Hash the password only if it's defined.
            if(user.password){
                User.hashPassword(user, callback);
            }else{
                callback();
            }

        });
    }

};
