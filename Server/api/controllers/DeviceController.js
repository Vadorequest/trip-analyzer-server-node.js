/**
 * DeviceController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests about devices.
 */

module.exports = {

    /**
     * Create a new device.
     * @param req
     * @param res
     */
    create: function (req, res) {
        validator.check({
            'required' : [
                validator.rulesUserSessionId(req.session.user.id),
                validator.rulesLabel(req.param('label')),
                validator.rulesHardwareId(req.param('hardwareId')),
            ],
            'optional' : [
                validator.rulesRefreshFrequency(req.param('refreshFrequency')),
                validator.rulesAskingFrequency(req.param('askingFrequency')),
                validator.rulesAskingFrequencyActive(req.param('askingFrequencyActive'))
            ]
        }, function(errors, valuesChecked){
            Device.create(objectHelper.merge(valuesChecked, {userId: req.session.user.id}), function(err, device){
                if(err){
                    res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(device){
                    res.json({message: 'Device created.', data: {device: device}, status: true});
                }else{
                    res.json({message: 'Error.', data: {device: device, code: '1100'}, status: false});
                }
            });
        }, res);
    },

    /**
     * Update an existing device.
     * @param req
     * @param res
     */
    update: function (req, res) {
        validator.check({
            'required' : [
                validator.rulesHardwareId(req.param('hardwareId'))// Use the hardware id instead of the id, because it's easier from the Netduino.
            ],
            'optional' : [
                validator.rulesLabel(req.param('label')),
                validator.rulesEmpty(req.param('details'), 'details'),// No rules but we want to add it automatically in the valuesChecked.
                validator.rulesRefreshFrequency(req.param('refreshFrequency')),
                validator.rulesAskingFrequency(req.param('askingFrequency')),
                validator.rulesAskingFrequencyActive(req.param('askingFrequencyActive'))
            ]
        }, function(errors, valuesChecked){
            if(!_.isEmpty(valuesChecked)){
                Device.findOne({hardwareId: valuesChecked.hardwareId}, function(err, device){
                    if(err){
                        return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                    }

                    if(device = dbHelper.merge(device, valuesChecked)){
                        device.save(function(err){
                            if(err){
                                return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                            }

                            return res.json({message: 'Device updated.', data: {device: device}, status: true});
                        });
                    }else{
                        return res.json({message: 'Device don\'t exists.', data: {}, status: false});
                    }
                });
            }else{
                return res.json({message: 'No values send. No change.', data: {device: req.session.device}, status: true});
            }
        }, res);
    },

    /**
     * Get all device which belong to the current connected user.
     * @param req
     * @param res
     */
    find: function(req, res){
        validator.check({
            'required' : [
                validator.rulesUserSessionId(req.session.user.id)
            ],
            'optional' : [
                validator.rulesId(req.param('id'), 'id', 'ID of device can\'t be empty. (optional)'),
                validator.rulesHardwareId(req.param('hardwareId')),
                validator.rulesLabel(req.param('label')),
                validator.rulesLimit(req.param('limit'))
            ]
        }, function(errors, valuesChecked){
            var query = Device.find(objectHelper.merge(valuesChecked, {userId: req.session.user.id}));

            req.param('limit') ? query.limit(req.param('limit')) : '';

            query.exec(function(err, device){
                if(err){
                    return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(device){
                    return res.json({message: 'Here are your own devices.', data: {device: device}, status: true});
                }else{
                    return res.json({message: 'User don\'t exists.', data: {}, status: false});
                }
            });
        }, res);
    },

    /**
     * Destroy a device. Disabled.
     * @param req
     * @param res
     */
    destroy: function (req, res) {
        res.json({message: 'This feature is disabled.', data: {}, status: false});
    }
  

};
