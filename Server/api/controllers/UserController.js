/**
 * UserController
 *
 * @module		:: Controller
 * @description	:: Contains logic for user management.
 */

module.exports = {

    /**
     * Check if the email and the password match and connect the user.
     * @param req
     * @param res
     */
    connect: function (req, res) {
        validator.check([
            validator.rulesEmail(req.param('email')),
            validator.rulesPassword(req.param('password'))
        ], function(errors, valuesChecked){
            User.exists({email: valuesChecked.email}, function(err, user){
                if(err){
                    return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(user){
                    bcrypt.compare(valuesChecked.password, user.password, function(err, result) {
                        if(err){
                            res.json({message: 'Error.', data: {code: '99', err: err}, status: false});
                        }

                        if(result === true){
                            req.session.authenticated = true;
                            req.session.user = user;
                            req.session.from = 'user';

                            return res.json({message: 'You are now connected. Enjoy it.', data: {user: user}, status: true});
                        }else{
                            return res.json({message: 'Bad password.', data: {code: '1002'}, status: false});
                        }
                    });
                }else{
                    return res.json({message: 'This user don\'t exists.', data: {code: '1001'}, status: false});
                }
            });
        }, res);
    },

    /**
     * Create a new user.
     * @param req
     * @param res
     */
    create: function (req, res) {
        validator.check([
            validator.rulesEmail(req.param('email')),
            validator.rulesPassword(req.param('password'))
        ], function(errors, valuesChecked){
            User.create(valuesChecked, function(err, user){
                if(err){
                    return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(user){
                    return res.json({message: 'User created.', data: {user: user}, status: true});
                }else{
                    return res.json({message: 'Error.', data: {user: user, code: '1100'}, status: false});
                }
            });
        }, res);
    },

    /**
     * Update the user connected.
     * @param req
     * @param res
     */
    update: function (req, res) {
        validator.check({
            required: [
                validator.rulesUserSessionId(req.session.user.id)
            ],
            optional: [
                validator.rulesEmail(req.param('newEmail')),
                validator.rulesPassword(req.param('newPassword'))
            ]
        }, function(errors, valuesChecked){
            if(!_.isEmpty(valuesChecked)){
                User.findOne(req.session.user.id, function(err, user){
                    if(err){
                        return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                    }

                    if(user = dbHelper.merge(user, valuesChecked, ['password'])){
                        user.save(function(err){
                            if(err){
                                return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                            }

                            return res.json({message: 'User updated.', data: {user: user}, status: true});
                        });
                    }else{
                        return res.json({message: 'User don\'t exists.', data: {}, status: false});
                    }
                });
            }else{
                return res.json({message: 'No values send. No change.', data: {user: req.session.user}, status: true});
            }
        }, res);
    },

    /**
     * Get the information about the current connected user.
     * @param req
     * @param res
     */
    find: function(req, res){
        validator.check({
            'required' : [
                validator.rulesUserSessionId(req.session.user.id)
            ],
            'optional' : [
                validator.rulesLimit(req.param('limit'), 'limit')
            ]
        }, function(errors, valuesChecked){
            User.findOne(objectHelper.merge(valuesChecked, {id: req.session.user.id}), function(err, user){
                if(err){
                    return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(user){
                    return res.json({message: 'Here are your own information.', data: {user: user}, status: true});
                }else{
                    return res.json({message: 'User don\'t exists.', data: {}, status: false});
                }
            });
        }, res);
    },

    /**
     * Destroy an user. Disabled.
     * @param req
     * @param res
     */
    destroy: function (req, res) {
        res.json({message: 'This feature is disabled.', data: {}, status: false});
    }


};
