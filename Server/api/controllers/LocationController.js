/**
 * LocationController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

module.exports = {

    /**
     * Create a new location.
     * @param req
     * @param res
     */
    create: function (req, res) {
        validator.check([
            validator.rulesHardwareId(req.param('hardwareId')),
            validator.rulesLat(req.param('lat')),
            validator.rulesLong(req.param('long'))
        ], function(errors, valuesChecked){
            Location.create(valuesChecked, function(err, location){
                if(err){
                    res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(location){
                    res.json({message: 'Location created.', data: {location: location}, status: true});
                }else{
                    res.json({message: 'Error.', data: {location: location, code: '1100'}, status: false});
                }
            });
        }, res);
    },

    /**
     * Get all device's position which belong to the current connected user.
     * @param req
     * @param res
     */
    find: function(req, res){
        validator.check({
            'required' : [
                validator.rulesHardwareId(req.param('hardwareId'))// Use the hardware id instead of the id, because it's easier from the Netduino.
            ],
            'optional' : [
                validator.rulesId(req.param('id'), 'id', 'ID of location can\'t be empty. (optional)'),
                validator.rulesLimit(req.param('limit'))
            ]
        }, function(errors, valuesChecked){
            var query = Location.find(valuesChecked).sort('createdAt desc');

            req.param('limit') ? query.limit(req.param('limit')) : '';

            query.exec(function(err, location){
                if(err){
                    return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(location){
                    return res.json({message: 'Here are your own location for the device.', data: {location: location}, status: true});
                }else{
                    return res.json({message: 'User don\'t exists.', data: {}, status: false});
                }
            });
        }, res);
    }
  

};
