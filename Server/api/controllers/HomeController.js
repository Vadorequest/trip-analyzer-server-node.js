/**
 * HomeController
 *
 * @module		:: Controller
 * @description	:: Home page.
 */

module.exports = {
    index: function (req, res) {
        return res.view({
            layout: 'layout',
            title: 'Trip Analyzer'
        });
    }
};
