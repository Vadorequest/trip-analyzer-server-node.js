/**
 * ActionController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

module.exports = {

    /**
     * Create a new action.
     * @param req
     * @param res
     */
    create: function (req, res) {
        validator.check({
            'required' : [
                validator.rulesHardwareId(req.param('hardwareId')),// Use the hardware id instead of the id, because it's easier from the Netduino.
                validator.rulesType(req.param('type')),
            ],
            'optional' : [
                validator.rulesData(req.param('data'))
            ]
        }, function(errors, valuesChecked){
            Action.create(valuesChecked, function(err, action){
                if(err){
                    res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(action){
                    res.json({message: 'Action created.', data: {action: action}, status: true});
                }else{
                    res.json({message: 'Error.', data: {action: action, code: '1100'}, status: false});
                }
            });
        }, res);
    },

    /**
     * Update an existing action.
     * @param req
     * @param res
     */
    update: function (req, res) {
        validator.check({
            'required' : [
                validator.rulesId(req.param('id'), false, 'The action to update is required. (ID)')// Key to false because we don't want to like this field to the model.
            ],
            'optional' : [
                validator.rulesStatus(req.param('status')),
                validator.rulesData(req.param('data'))
            ]
        }, function(errors, valuesChecked){
            if(!_.isEmpty(valuesChecked)){
                Action.findOne({id: req.param('id')}, function(err, action){
                    if(err){
                        return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                    }

                    if(action){
                        action = dbHelper.merge(action, valuesChecked);
                        action.save(function(err){
                            if(err){
                                return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                            }

                            return res.json({message: 'Action updated.', data: {action: action}, status: true});
                        });
                    }else{
                        return res.json({message: 'Action don\'t exists.', data: {}, status: false});
                    }
                });
            }else{
                return res.json({message: 'No values send. No change.', data: {action: req.session.action}, status: true});
            }
        }, res);
    },

    /**
     * Get all actions which belong to the current connected user.
     * @param req
     * @param res
     */
    find: function(req, res){
        validator.check({
            'required' : [
                validator.rulesHardwareId(req.param('hardwareId'))// Use the hardware id instead of the id, because it's easier from the Netduino.
            ],
            'optional' : [
                validator.rulesId(req.param('id'), 'id', 'ID of action can\'t be empty. (optional)'),
                validator.rulesStatus(req.param('status')),
                validator.rulesLimit(req.param('limit'))
            ]
        }, function(errors, valuesChecked){
            var query = Action.find(valuesChecked);

            req.param('limit') ? query.limit(req.param('limit')) : '';

            query.exec(function(err, action){
                if(err){
                    return res.json({message: 'DB error.', data: {code: '100', err: err}, status: false});
                }

                if(action){
                    return res.json({message: 'Here are the actions.', data: {action: action}, status: true});
                }else{
                    return res.json({message: 'User don\'t exists.', data: {}, status: false});
                }
            });
        }, res);
    },

    /**
     * Destroy an action. Disabled.
     * @param req
     * @param res
     */
    destroy: function (req, res) {
        res.json({message: 'This feature is disabled.', data: {}, status: false});
    }
};
