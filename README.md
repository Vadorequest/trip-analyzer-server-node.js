### Trip Analyzer ###


[Project hosted:](https://bitbucket.org/Vadorequest/trip-analyzer-server-node.js)

Hi.
The goal of our application is to track a device (Netduino) and get its location on a Windows Phone 8 using a web service hosted with Node.js.
Could work with many Netduinos and many phones.
The project is separated in several parts, three at all.
All the documentation about all the parts are located in the **/Documentation** folder. *Merise, Mockups, UML, more...*
The three parts are:

+ **Server**: Server side, [Node.js](http://nodejs.org/) application using [Sails.js](http://sailsjs.org/#!) hosted on a Virtual Machine (*Ubuntu 13.04*) for the demo, provided by Azure.
+ **Phone**: Client side, Windows Phone 8 application used for control the Netduino part using the Server for host shared data.
+ **Netduino**: Client side, device which send his own location to the *Server* and execute action asked by the Phone.

The source code is *public*, because it's only a demonstration and not something that we can use in production.

The WP8 application is badly designed, we make it in about one day and didn't do another WP8 application before.
It was done really quiclkly and it's not really a good design, we should use more MVVM and our way to bind data is bad. 
So, don't use our WP8 source code for make your own application, it will make it you *crazy*!
*But it works.*

The Netduino doesn't had a true GPS, it uses only fake GPS location yet. But we could improve it later, we are using a GPS designed for Arduino and it's not really compatible.

####Video presentation####
You can see a video presentation [here](https://www.youtube.com/watch?v=FGUHEaGeqZ0) about the project (*11mn*).


The project was realized by French developpers:
=====================================================

+ Ambroise Dhenain: Project manager, server developper, Windows Phone 8.
+ Julien Polizzi: Netduino developper, Windows Phone 8.
