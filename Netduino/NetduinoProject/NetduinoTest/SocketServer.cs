using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using System.IO;

namespace TripAnalyserNetduino
{
    class SocketServer
    {


        // setup the LED and turn it off by default
        OutputPort led = new OutputPort(Pins.ONBOARD_LED, false);
        // configure the port # (the standard web server port is 80)
        int port = 80;
        const Int32 c_microsecondsPerSecond = 1000000;

        public void Start()
        {
            // wait a few seconds for the Netduino Plus to get a network address.
            Thread.Sleep(5000);
            // display the IP address
            Microsoft.SPOT.Net.NetworkInformation.NetworkInterface
            networkInterface =
            Microsoft.SPOT.Net.NetworkInformation.NetworkInterface.
            GetAllNetworkInterfaces()[0];
            Debug.Print("my ip address: " + networkInterface.IPAddress.ToString());

            ThreadStart listenDelegate = new ThreadStart(this.listen);
            Thread listenThread = new Thread(listenDelegate);
            listenThread.Start();

            ThreadStart sendDelegate = new ThreadStart(this.sendLocation);
            Thread sendThread = new Thread(sendDelegate);
            sendThread.Start();
        }


        public void listen()
        {
            // create a socket to listen for incoming connections
            Socket listenerSocket = new Socket(AddressFamily.InterNetwork,
            SocketType.Stream,
            ProtocolType.Tcp);
            IPEndPoint listenerEndPoint = new IPEndPoint(IPAddress.Any, port);
            // bind to the listening socket
            listenerSocket.Bind(listenerEndPoint);
            // and start listening for incoming connections
            listenerSocket.Listen(1);
            // listen for and process incoming requests
            while (true)
            {
                try
                {
                    // wait for a client to connect
                    Socket clientSocket = listenerSocket.Accept();
                    // wait for data to arrive
                    bool dataReady = clientSocket.Poll(5000000, SelectMode.SelectRead);
                    // if dataReady is true and there are bytes available to read,
                    // then you have a good connection.
                    if (dataReady && clientSocket.Available > 0)
                    {
                        byte[] buffer = new byte[clientSocket.Available];
                        int bytesRead = clientSocket.Receive(buffer);
                        string request =
                        new string(System.Text.Encoding.UTF8.GetChars(buffer));
                        Debug.Print("---- " + request + " ----");
                        if (request.IndexOf("ON") >= 0)
                        {
                            led.Write(true);
                        }
                        else if (request.IndexOf("OFF") >= 0)
                        {
                            led.Write(false);
                        }
                        string statusText = "LED is " + (led.Read() ? "ON" : "OFF") + ".";
                        // return a message to the client letting it
                        // know if the LED is now on or off.
                        string response =
                        "HTTP/1.1 200 OK\r\n" +
                        "Content-Type: text/html; charset=utf-8\r\n\r\n" +
                        "<html><head><title>Netduino Plus LED Sample</title></head>" +
                        "<body>" + statusText + "</body></html>";
                        clientSocket.Send(System.Text.Encoding.UTF8.GetBytes(response));
                    }
                    // important: close the client socket
                    clientSocket.Close();
                }
                catch (System.Net.Sockets.SocketException e)
                {
                    Debug.Print(e.StackTrace);
                    Debug.Print(e.Message);
                }
            }
        }

        public void sendLocation()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://137.135.176.144:1337/user/connect?email=julien.polizzi@gmail.com&password=ju");
            request.Method = "GET";
            HttpWebResponse httpResp = (HttpWebResponse)request.GetResponse();
            Stream responseStream = httpResp.GetResponseStream();
            Byte[] httpBuffer = new Byte[2048];
            responseStream.Read(httpBuffer, 0, 2048);
            Debug.Print(new String(System.Text.Encoding.UTF8.GetChars(httpBuffer)));

            IPEndPoint sendEndPoint = new IPEndPoint(IPAddress.Parse("137.135.176.144"), 1337);
            while (true)
            {
                try
                {
                    //Send data through socket

                    Socket sendSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    sendSocket.Connect(sendEndPoint);
                    int i = sendSocket.Send(System.Text.Encoding.UTF8.GetBytes("GET http://137.135.176.144:1337/user/connect/julien.polizzi@gmail.com/ju"));
                    Debug.Print(i.ToString());
                    
                    Byte[] buffer = new Byte[1024];
                    String response = String.Empty;

                    DateTime timeoutAt = DateTime.Now.AddSeconds(15);
                    while (sendSocket.Available == 0 && DateTime.Now < timeoutAt)
                    {
                        System.Threading.Thread.Sleep(100);
                    }

                    while (sendSocket.Poll(15 * c_microsecondsPerSecond,
                    SelectMode.SelectRead))
                    {
                        if (sendSocket.Available == 0)
                        break;
                        Array.Clear(buffer, 0, buffer.Length);
                        sendSocket.Receive(buffer);
                        response = response + new String(System.Text.Encoding.UTF8.GetChars(buffer));
                    }
                    Debug.Print(response);
                    sendSocket.Close();
                }
                catch (System.Net.Sockets.SocketException e)
                {
                    Debug.Print(e.Message + " Error code: {1}." + e.ErrorCode);
                }

            }
        }
    }
}
