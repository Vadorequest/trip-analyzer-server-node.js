using System;
using System.Threading;
using Microsoft.SPOT;
using Toolbox.NETMF.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using Microsoft.SPOT.Hardware;
using Toolbox.NETMF;

namespace TripAnalyserNetduino
{
    class LocationManager
    {
        private float longitude = long.MinValue;
        private float latitude = long.MinValue;
        public static NmeaGps Gps = new NmeaGps("COM3",9600);
        public static OutputPort Led = new OutputPort(Pins.ONBOARD_LED, false);

        public LocationManager()
        {
            
            Gps.GotFix += new NativeEventHandler(Gps_GotFix);
            Gps.LostFix += new NativeEventHandler(Gps_LostFix);
            Gps.PositionChanged += new NativeEventHandler(Gps_PositionChanged);
            // Starts the GPS device
            Debug.Print("Trying to get a fix...");
            Gps.Start();
            //while (true)
            //{
            //    Debug.Print(Gps.BytesAccepted + " bytes accepted, " + Gps.BytesDiscarded + " bytes discarded");
            //    Thread.Sleep(1000);
            //}
        }


        static void Gps_PositionChanged(uint Unused, uint FixType, DateTime GPSTime)
        {
            string Outp = "";
            Outp += "3D-Fix: " + Gps.Fix3D.ToString();
            Outp += ", Sattellites: " + Gps.Satellites.ToString();
            Outp += ", Time: " + Gps.GPSTime.ToString();
            Outp += ", Latitude: " + Gps.SLatitude;
            Outp += ", Longitude: " + Gps.SLongitude;
            Outp += ", Altitude: " + Gps.SAltitude;
            Outp += ", Knots: " + Gps.Knots.ToString() + " (" + Gps.Kmh.ToString() + " km/h)";
            Debug.Print(Outp);

            // If you want to translate this to a Bing Maps URL, try this:
            Debug.Print("http://www.bing.com/maps/?q=" + Tools.RawUrlEncode(Gps.Latitude.ToString() + " " + Gps.Longitude.ToString()));
        }

        static void Gps_GotFix(uint Unused, uint FixType, DateTime GPSTime)
        {
            Debug.Print("We got a fix, yay!!");
        }

        static void Gps_LostFix(uint Unused, uint FixType, DateTime GPSTime)
        {
            Debug.Print("We lost our GPS fix :(");
        }


        public void setLongitude(float longitude)
        {
            this.longitude = longitude;
        }
        public float getLongitude()
        {
            return this.longitude;
        }

        public void setLatitude(float latitude)
        {
            this.latitude = latitude;
        }
        public float getLatitude()
        {
            return this.latitude;
        }
    }
}
