using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.IO;
using System.Collections;

namespace TripAnalyserNetduino
{
    class WebServer
    {
        private String url = "http://137.135.176.144:1337/";
        private String hardwareId = String.Empty; //MAC Address
        private int sendDelay = 30; //in seconds
        private int askDelay = 15;  //in seconds
        private bool random = true;
        private bool predefined = false;

        private LocationManager locationManager;

        private ArrayList predefinedList;
        private int listPos = 0;

        //Starts both the listening and location sending thread
        public void Start()
        {
            locationManager = new LocationManager();
            // wait a few seconds for the Netduino Plus to get a network address.
            Thread.Sleep(5000);
            // display the IP address
            Microsoft.SPOT.Net.NetworkInformation.NetworkInterface networkInterface = Microsoft.SPOT.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()[0];
            Debug.Print("my ip address: " + networkInterface.IPAddress.ToString() + "\n");
            byte[] bytes = networkInterface.PhysicalAddress;
            for (int i = 0; i < bytes.Length; i++)
            {
                hardwareId = hardwareId + bytes[i].ToString("X2");
            }
            Debug.Print("my MAC address: " + hardwareId);

            setPredefinedList(); //Set the predefined list of locations

            //Make sure the device exists in the database and create it if it doesn't
            if (!findDevice())
            {
                bool created = false;
                while (!created)
                {
                    created = createDevice();
                }
            }

            ThreadStart listenDelegate = new ThreadStart(this.Listen);
            Thread listenThread = new Thread(listenDelegate);
            listenThread.Start();

            ThreadStart sendDelegate = new ThreadStart(this.SendLocation);
            Thread sendThread = new Thread(sendDelegate);
            sendThread.Start();
        }

        /// <summary>
        /// Queries the server at a regular interval checking for actions to perform
        /// and updates the refresh times
        /// </summary>
        public void Listen()
        {
            while (true)
            {
                //Make sure the devices exists and updates the refresh and ask delays
                bool deviceFound = findDevice();
                if (deviceFound)
                {
                    Hashtable result = queryServer("action/find?status=Pending&hardwareId=" + hardwareId );
                    if (result != null && result.Contains("status") && (bool)result["status"] 
                        && result.Contains("data") && result["data"] != null) 
                    {
                        Hashtable data = (Hashtable)result["data"];
                        if (data["action"] != null)
                        {
                            ArrayList actions = (ArrayList)data["action"];
                            for (int i = actions.Count - 1; i >= 0; i--)
                            {
                                Hashtable action = (Hashtable)actions[i];
                                String type = (String)action["type"];
                                switch (type)
                                {
                                    case "testRandom":
                                        this.predefined = false;
                                        this.random = true;
                                        action = queryServer("action/update?hardwareId="+hardwareId+ "&id=" + (String)action["id"] + "&status=Executed");
                                        break;
                                    case "testPredefined":
                                        this.predefined = true;
                                        this.random = false;
                                        action = queryServer("action/update?hardwareId=" + hardwareId + "&id=" + (String)action["id"] + "&status=Executed");
                                        break;
                                    case "refreshLocation":
                                        if (!random && !predefined && (locationManager.getLatitude() != float.MinValue 
                                            || locationManager.getLongitude() != float.MinValue))
                                        {
                                            action = queryServer("location/create?hardwareId=" + hardwareId + "&lat=" + locationManager.getLatitude().ToString() + "&long=" + locationManager.getLongitude().ToString());
                                        }
                                        break;
                                    default:
                                        action = queryServer("action/update?hardwareId=" + hardwareId + "&id=" + (String)action["id"] + "&status=Executed");
                                        break;
                                }
                            }
                        }
                    }

                }
                Thread.Sleep(askDelay * 1000);
            }
        }

        /// <summary>
        /// Sends a gps location to the webserver at a regular interval
        /// </summary>
        public void SendLocation()
        {
            while (true)
            {
                if (random)
                {
                    double lat = randomDouble(9.405710d, 70.945356d);
                    double lon = randomDouble(-7.382813d, 157.178438);
                    Hashtable result = queryServer("location/create?hardwareId=" + hardwareId + "&lat="+ lat + "&long=" + lon);
                    Debug.Print(lat.ToString() + ", " + lon.ToString());
                }
                else if (predefined)
                {
                    Hashtable result = queryServer(predefinedList[listPos].ToString());
                    listPos = listPos + 1;
                    if (listPos > predefinedList.Count - 1) { listPos = 0; }
                }
                else if (locationManager.getLatitude() != float.MinValue || locationManager.getLongitude() != float.MinValue)
                {
                    Hashtable result = queryServer("location/create?hardwareId=" + hardwareId + "&lat=" + locationManager.getLatitude().ToString() + "&long=" + locationManager.getLongitude().ToString());
                    if (result != null && result.Contains("status") && (bool)result["status"])
                    {

                    }
                }

                Thread.Sleep(sendDelay * 1000);
            }
        }

        /// <summary>
        /// Checks that this device exists in the database via the webservice.
        /// Also updates refresh times each times it's called.
        /// </summary>
        /// <returns>Whether the device was found or not</returns>
        public bool findDevice()
        {
            bool deviceFound = false;
            Hashtable result = queryServer("device/find?hardwareId=" + hardwareId);
            if (result != null && result.Contains("data"))
            {
                Hashtable data = (Hashtable)result["data"];
                if (data != null && data.Contains("device") && data["device"] != null) 
                { 
                    deviceFound = true;
                    ArrayList devices = (ArrayList)data["device"];
                    Hashtable device = (Hashtable)devices[0];
                    this.sendDelay = int.Parse(device["refreshFrequency"].ToString());
                    this.askDelay = int.Parse(device["askingFrequency"].ToString());
                    
                }

            }
            return deviceFound;
        }

        /// <summary>
        /// Creates this device in the database
        /// </summary>
        /// <returns>Wether the device was created or not</returns>
        public bool createDevice()
        {
            bool success = false;
            Hashtable result = queryServer("device/create?hardwareId=" + hardwareId + "&label=Netduino"+ hardwareId);

            if (result != null && result.Contains("status") && (bool)result["status"])
            {
                success = true;
            }

            return success;
        }

        /// <summary>
        /// Creates a httpwebrequest and queries the server with the given string as the query
        /// </summary>
        /// <param name="query">The query to the webserver (e.g.: device/find?hardwareId=5C864A008D3F)</param>
        /// <returns>The hashtable corresponding to the response from the webservice</returns>
        public Hashtable queryServer(String query)
        {
            Hashtable result = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + query);
                request.Method = "GET";
                HttpWebResponse httpResp = (HttpWebResponse)request.GetResponse();
                Thread.Sleep(500);
                Stream responseStream = httpResp.GetResponseStream();
                int length = int.Parse(responseStream.Length.ToString());
                Byte[] httpBuffer = new Byte[length];
                for (int i = 0; i < length; i++)
                {
                    httpBuffer[i] = (byte)responseStream.ReadByte();
                }
                //store the json response from the server as a string
                String json = new String(System.Text.Encoding.UTF8.GetChars(httpBuffer));
                Debug.Print(json);
                //parse the json string as an object
                result = (Hashtable)Json.NETMF.JsonSerializer.DeserializeString(json);
            }
            catch(Exception e)
            {
                Debug.Print(e.Message + "\n" + e.StackTrace);
            }
            return result;
        }

        private void setPredefinedList()
        {
            predefinedList = new ArrayList();
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.160372&long=10.130988");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.160306&long=10.129614");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.159745&long=10.128660");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.159347&long=10.127968");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.158690&long=10.126830");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.158259&long=10.126208");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.157399&long=10.125307");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.156598&long=10.124642");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.155834&long=10.124921");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.154973&long=10.126444");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.153993&long=10.127174");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.153479&long=10.129083");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.153718&long=10.131272");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.155081&long=10.131465");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.156120&long=10.130821");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.157100&long=10.130199");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.158068&long=10.129963");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.158498&long=10.132259");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.158654&long=10.134147");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.159777&long=10.133826");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.160291&long=10.133139");
            predefinedList.Add("location/create?hardwareId=" + hardwareId + "&lat=56.160386&long=10.131959");
        }

        private double randomDouble(double min, double max)
        {
            Random rnd = new Random();
            return rnd.NextDouble() * (max - min) + min;
        }
    }
}
